﻿using UnityEngine;
using System.Collections;

public class TuxControllerScript : MonoBehaviour {

	public float maxSpeed = 10;
	bool facingRight = true;
	public float JumpSpeed = 10.0f;
	bool canJump = true;

	Animator anim;

	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator>();
	}


	// When the character hits the ground, it can jump.
	void OnCollisionEnter(){
		canJump = true;
	}

	// Sets the can jump boolean upon touching the ground and upon leaving it
	void OnCollisionExit(){
		float delay = 0.5f;
		float counter = 0;
		Debug.Log("Trying to Disable!");
		while(counter < delay){
			counter += Time.deltaTime;
		}
		Debug.Log("Disabling!");
		canJump = false;

	}


	// This covers the jumping action
	void Jump(){
		if(canJump){
			rigidbody2D.AddForce(Vector3.up * JumpSpeed);
		}
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		// Gets the horzontal input
		float moveLeft = Input.GetAxis ("Horizontal");

		// Get the vertical input
		if(Input.GetButton("Jump")){
			Jump();

		}

		// Updates the animator speed variable, allowing for a transfer of animations
		anim.SetFloat ("HorizSpeed", Mathf.Abs (moveLeft));
		anim.SetFloat ("VertSpeed", Mathf.Abs (rigidbody2D.velocity.y));

		rigidbody2D.velocity = new Vector2(moveLeft * maxSpeed, rigidbody2D.velocity.y);
		if((moveLeft > 0 && !facingRight) || (moveLeft < 0 && facingRight)){
			Flip();
		}

		// Get the vertical velocity
		float vertVel = Mathf.Abs (rigidbody2D.velocity.y);
		anim.SetFloat ("VertSpeed", vertVel);

	}
		
	void Flip(){
		facingRight = !facingRight;
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}
}
